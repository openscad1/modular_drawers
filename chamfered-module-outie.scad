

module mod() {
    translate([-30, -30, -35]) {
        color("red") {
            translate([30,-3.75,-0.4417]) {
                import("lib/Interlocking_Module.stl");
            }
        }

        color("blue") {
            //cube([60, 60, 70]);
        }
    }
}







module foo () {
    for(x = [-1, 1]) {
        for(y = [-1, 1]) {
            translate([x * 30, y * 30, 0]) {
                rotate([0, 0, x * y * 45]) {
                    color("pink") cube([10, 20, 100], center = true);
                }
            }
        }
    }
}


module bar () {
    for(x = [-1, 1]) {
        for(y = [-1, 1]) {
            translate([x * 35, y * 35, 0]) {
                rotate([0, 0, x * y * 45]) {
                    color("pink") cube([10, 20, 100], center = true);
                }
            }
        }
    }
}


module baz() {
    difference() {
        mod();
        rotate([90, 0, 0]) bar();
        rotate([0, 90, 0]) bar();
    }
}
    
    
module qux() {
   intersection() {
        baz();    
        translate([0, 35.01, 0]) {
            color("steelblue") {
                cube([10,10,70], center=true);
            }
        }
    }
}



scaling = 50.8 / 60;

scale([scaling, scaling, scaling]) {
    qux();
    rotate([0, 0, 180]) {
        qux();
    }
}


